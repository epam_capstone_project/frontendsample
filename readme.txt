# Sample UI for EPAM project
This is a sample UI of my EPAM capstone project.
UI will be based on `HTML`, `CSS` and `Javascript`.
There's no any request to back-end, pages are static, displayed data is artificial.
To run the UI, just open `index.html` on browser.